{
  rvorg ? builtins.fetchGit ./.
}:

with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rvorg";
  version = "rvorg${toString rvorg.revCount}_${rvorg.shortRev}";

  src = rvorg;

  installPhase = ''
    # Build the site to the $out directory
    ${pkgs.python27Packages.pelican}/bin/pelican content/ -o $out -t theme/
    chmod -R +x $out
  '';
}

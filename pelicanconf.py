#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
INDEX_SAVE_AS = 'najave.html'

AUTHOR = u'razmjena vje\u0161tina'
SITENAME = u'Razmjena vje\u0161tina'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'hr'

# Feed generation is usually not desired when developing
FEED_ATOM = 'feeds/atom.xml'
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),)

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

THEME = "./theme"

DESCRIPTION = ''
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Najave Razmjene vještina

Repozitorij za sajt <https://razmjenavjestina.org/>.

Sajt je konfiguriran da svakih 15 minuta povuče sadržaj iz ovog repozitorija.

## Nove objave

Novi sadržaj dodati u `content/` direktorij u Markdown ili RST formatu,
commitati i gurnuti u ovaj remote.

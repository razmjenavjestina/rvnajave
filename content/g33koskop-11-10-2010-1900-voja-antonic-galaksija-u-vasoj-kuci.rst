G33koskop (12.10.2010. @19:00): Voja Antonić: Galaksija u vašoj kući
####################################################################
:date: 2010-10-09 00:24
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-11-10-2010-1900-voja-antonic-galaksija-u-vasoj-kuci
:status: published

Kada smo na g33koskopu krenuli sa serijom predavanja o povijesti
tehničke kulture *naših prostora*\ ™ stalno se na vrhu te liste
pojavljivao Voja Antonić. I stoji tako dugo na vrhu. Do ovog utorka
12.10.10. (epic) u 19:00, `klub mama, Preradovićeva
18 <http://maps.google.com/maps?q=klub+mama&ie=UTF8&hq=klub+mama&hnear=&ll=45.810136,15.974226&spn=0.001038,0.002409&t=h&z=19&iwloc=A>`__
(standardno).

| 1984. u uvodnom tekstu drugog broja "Računara u vašoj kući" stajalo
  je: "`Računarsku revoluciju, najzad, možemo ostvariti samo ako budemo
  imali domaći
  računar. <http://www.dejanristanovic.com/refer/lupali.htm>`__" Voja
  Antonić je to doslovno shvatio. Dizajnirao je računalo da u svakom
  domu lako izraste po jedna
  `Galaksija <http://en.wikipedia.org/wiki/Galaksija>`__. I izraslo je
  10000 samogradnji. Minimum. Fascinatna
| priča. Na g33koskpu. Hell fucking yeah! :)

Kaže Voja za g33koskopsku najavu: "Krajem 70-tih godina računar je bio u
domenu magije, pa smrtnici nisu znali ni kako on izgleda ni čemu služi.
Prve projekte sa mikroprocesorima sam radio dok još nisam ni video
mikroračunar, program sam asemblirao ručno na papiru i EPROM programirao
DIY programatorom, tako što sam tri minuta držao pritisnut jedan taster
na njemu, jer dok sam ga pravio, nisam znao na šta će ličiti i nisam
imao dovoljno imaginacije da predvidim prekidač za programiranje.

Nisam slutio kuda će to odvesti. Uostalom sve to i nije moja zaluga,
nego sam se u dobrom trenutku "namestio" medijima kojima je trebala
sveža krv.

Ubrzo sam shvatio da digitalna tehnika nije statičan fenomen i da se
prebrzo razvija da bi siromašan pojedinac mogao da prati tempo koji su
zadavale korporacije. Kako su se računari razvijali, moja Galaksija je
izgledala sve skromnije, pa sam u trenutku razočaranja bacio sve
prototipove u kontejner. Jedan je ostao slučajno u podrumu, mnogo
kasnije sam ga našao i odgovorio na molbu da ga poklonim Muzeju tehnike.
Sad je on eksponat u stalnoj postavci izložbe."

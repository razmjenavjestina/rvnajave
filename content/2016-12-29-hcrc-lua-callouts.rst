HCRČ: Lua C API vs. FFI (29.12.2016. @ 19:30)
#############################################
:date: 2016-12-29 12:22
:author: nikolap
:category: hcrc
:status: published

U blagdanskom izdanju HCRČ-a ćemo se baviti performansama callout tehnika koje
nam nudi Lua(JIT). Je li bolje extendati ili embedati? C API ili FFI? Laž,
velika laž ili benchmark? Nakon HCRČ-a bi sve moglo biti jasnije.

Kod će biti objavljen na našem `GitLabu <https://gitlab.com/razmjenavjestina>`__.

Počinjemo u 19:30 u Hacklabu u Mami. Vidimo se!

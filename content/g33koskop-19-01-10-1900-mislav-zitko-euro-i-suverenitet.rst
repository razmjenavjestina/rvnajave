G33koskop (19.01.10. @ 19:00): Mislav Žitko: Euro i suverenitet
###############################################################
:date: 2010-01-18 17:24
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-19-01-10-1900-mislav-zitko-euro-i-suverenitet
:status: published

Nakon Viktora s pozamašnim naramkom polja matematike, računalnih
znanosti, sistemske biologije koje bi trebalo znati prije napada na
koncept novca, pa Stevea McQueena kako razbija u pokeru, Mislav Žitko
govorit će o euru i suverenitetu:

"*Euro kao nadnacionalna valuta predstavlja fenomen u kojem se ogledaju
proturječja za koja držimo da su neizbježna u svakom monetarnom režimu
(unutar kapitalističkog načina proizvodnje).*

*Unutar ortodoksnog ekonomskog diskursa novac nije bio adekvatno obrađen
budući da za njega nije bilo mjesta u paradigmatskom primjeru ekonomskog
odnosa - jednstavnoj razmijeni ili trampi.*

*Teorija novca koja želi zaobići probleme vezane uz metodološki
individualizam, mora objasniti kolektivnu, odnosno društvenu dimenziju
novca, što znači da mora dovesti u pitanje postulat o neutralnosti
novca.*"

Razmjena vještina (14.07.2012. @ 15:30 h): CoffeeScript i Backbone.js, Lisp
###########################################################################
:date: 2012-07-13 19:04
:author: nplejic
:category: razmjenavjestina
:slug: razmjena-vjestina-14-07-2012-1530-h-coffeescript-i-backbone-js-lisp
:status: published

*Arhiva svih
videa: \ http://www.youtube.com/user/razmjenavjestina/videos*

I ove subote ćemo razmjenjivati vještine u Hacklabu u Mami, planom i
programom sličnim prošlotjednom.

15:30 -- Crowd Programming
--------------------------

U duhu `pair
programminga <http://en.wikipedia.org/wiki/Pair_programming>`__ krenuli
bismo sa zajedničkim radom gdje driver/vozač vozi na projektoru, a
promatrači navigiraju.

Točna tema još nije poznata. Prijedlozi dobrodošli.

17:30 -- Kazimir Majorinc: Lisp
-------------------------------

Nakon vrlo uspješnog prošlotjednog predavanja, Kazimir nastavlja sa
svojom serijom o Lispu. Teme izlaganja bi bile najvažniji materijali o
Lispu, uglavnom članci, ponekad knjige – koliko je moguće popularno i
historijskim redom, od kraja 1950-ih pa naovamo.

19:00 -- Nikola Plejić: CoffeeScript i Backbone.js
--------------------------------------------------

Browser se iz glupog terminala pretvorio u vrlo bogato okruženje
sposobno za brzo i stabilno izvršavanje kompleksnih aplikacija. Shodno
tome, u browseru se vrti sve više i više koda. Pogledat ćemo kako
CoffeeScript, jezik koji se kompajlira u JavaScript, te Backbone.js,
MVC-oliki framework za browser, mogu doprinijeti bržem i jednostavnijem
razvoju tih aplikacija.

20:00 -- Vladimir Klemo: priprava i uživanje u hrani
----------------------------------------------------

Nakon svega ćemo uz pomoć Kleme pokušati nešto skuhati i uživati u
dotičnom.

Predavanja će biti snimana i streamana na Google+ Hangoutu. Više
informacija snimkama i streamu naknadno.

Snimke od prošlog tjedna možete pogledati u `playlisti na YouTube kanalu
Razmjene
vještina <https://www.youtube.com/playlist?list=PL3219BD58C1F0FB3E&feature=plcp>`__.

G33koskop seminar (03.12.2011. @15:00): O arhivi i knjižnici
############################################################
:date: 2011-12-01 04:35
:author: marcell
:category: Uncategorized
:slug: g33koskop-seminar-03-12-2011-1500-o-arhivi-i-knjiznici
:status: published

Dusan Barok je godinama prikupljao građu o medijskoj umjetnosti u
Istočnoj i Centralnoj Evropi. Sto gigabajta. Hiljade bibliografskih
zapisa. Ako Dušanovu medijsku arhivu ikad nađete na ThePirateBay.com
činit će se ilegalnom bombo(no)m koju treba što prije ukrasti, ako
naletite na njeno otvaranje na kuriranoj izložbi u lokalnoj galeriji
prošetat ćete njome s palcem na bradi, ako vam Dušan na vrhu svog
websajta ponudi dobar search bar sa shortcutom na bibtex referencu pisat
ćete doktorat, ako dođete na g33koskop seminar o knjižnicama... ako
dođete...

| Program:
| 15:00 - Marcell Mars: Internet kao knjižnica, kibernaut kao knjižničar
| 16:00 - Dobrica Pavlinušić: Knjižnica danas
| 17:00 - Dusan Barok: Moja, tvoja, naša arhiva

| Dusan Barok -
  http://www.burundi.sk/monoskop/index.php/Du%C5%A1an_Barok
| Dobrica Pavlinušić - http://www.rot13.org/~dpavlin/personal.html
| Marcell Mars - http://ki.ber.kom.uni.st

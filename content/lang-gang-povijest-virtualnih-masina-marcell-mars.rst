Lang Gang (13.05.2012. @ 18:00 h): Povijest virtualnih mašina - Marcell Mars
############################################################################
:date: 2012-05-12 00:01
:author: nplejic
:category: Lang Gang
:slug: lang-gang-povijest-virtualnih-masina-marcell-mars
:status: published

U ovomjesečnom izdanju Lang Ganga koje će se održati 13. svibnja,
`Marcell <http://ki.ber.kom.uni.st/>`__ će pričati o povijesti i razvoju
virtualnih mašina. Bit će govora o Smalltalku, LISP-u, JVM-u i inima, a
sve je bazirano na Marcellovim člancima izašlima u `časopisu Tracing
Concepts <http://www.scribd.com/doc/83101163/Tracing-Concepts-Volume-Issue-28>`__
(pod naslovima *Beirdo* i *Programming*).

Početak je u standardnih 18 h. Više o Lang Gangu:
`ovdje <http://wiki.razmjenavjestina.org/index.php/LangGang>`__.

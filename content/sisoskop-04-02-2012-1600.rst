Sys'o'skop (04.02.2012. @16:00)
###############################
:date: 2012-02-04 12:19
:author: bud
:category: Uncategorized
:tags: najava, najave, sysadmin
:slug: sisoskop-04-02-2012-1600
:status: published

Pozdrav svima,

| neki možda znaju, neki su samo načuli, no eto krećemo s novim
  programom u Mami koji smo zasad mislili uvalit u sklopu `razmjene
  vještina <http://www.razmjenavjestina.org>`__.
| Kako  se sad već okupio neki broj sistemaca, koji već imaju neka
  iskustava u sistemašenju te i ponešto znanja ;), rekli smo ajmo to
  nešto malo podjelit sa svijetom. Pa eto, ako netko želi čuti nešto o
  temama koje predlažemo na
  `wiki-u <http://wiki.razmjenavjestina.org/index.php/Sys%27o%27skop>`__
  navratite danas od 16:00 u Hacklab u Mami.

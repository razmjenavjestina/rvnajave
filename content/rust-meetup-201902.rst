impl Zagreb for Rust (07.02.2019. @ 18:00 h)
############################################
:date: 2019-02-02 11:42
:author: nikola
:category: Uncategorized
:tags: hcrc, najava
:slug: rust-meetup-201902
:status: published

Razmjena kreće s Rust meetupovima. Prvi nam nastupa Aka s kratkim pregledom
njegovog puta od C++ programera do Rust programera.

Ovime ujedno i najavljujemo redovna druženja Rust programera u Zagrebu gdje
ćemo zajedno istraživati jezik na svim razinama. Za sljedeći put najavljujemo
ponešto o razvoju web servisa.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/258460835/>`__.

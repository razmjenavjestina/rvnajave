Lang Gang: OGRE & libhand - Marin Šarić
#######################################
:date: 2012-04-01 10:09
:author: nplejic
:category: Uncategorized
:slug: lang-gang-ogre-libhand-marin-saric
:status: published

Na prvoaprilskom izdanju Lang Ganga imamo prilike poslušati ponešto o
`OGRE <http://www.ogre3d.org/>`__-u, popularnom open source 3D rendering
engineu, i `libhandu <http://www.libhand.org/>`__, libraryju za
renderiranje i prepoznavanje artikulacija ljudske ruke.

Predavat će Marin Šarić, autor libhand libraryja, s početkom u
standardnih 18 h.

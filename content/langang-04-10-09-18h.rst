Langang (04.10.09. @18h)
########################
:date: 2009-10-12 13:56
:author: admin
:category: Uncategorized
:tags: langgang, najava
:slug: langang-04-10-09-18h
:status: published

Dakle, u nedjelju je sljedeći, 11. po redu langgang. Zbog iznenadnih
promjena programa sada imamo "samo" Dobricu sa jako velikom temom.
Počinje sa CouchDB-om pa ide preko slične tehnologije razvijene u Frey-u
sve do vlastitih skalabilnih distribuiranih in-memory baza . Ukoliko još
netko ima neku manju temu (cca 30 min) može je predložiti na licu
mjesta.

G33koskop (05.01.10. @ 19:00): Viktor Winschel - Ekonomija ekonomije
####################################################################
:date: 2010-01-04 21:36
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-05-01-10-1900-viktor-winschel-ekonomija-ekonomije
:status: published

`Viktor Winschel <http://www.vwl.uni-mannheim.de/winschel/>`__ ne zna
što je to novac i iako je doktorirao ekonomiju ne želi trošiti vrijeme
na akumuliranje tako neke neopipljivo neobjašnjive stvari. Ili ne. No da
ne bi razočarao sve one sa snovima o akumuliranju, Viktor se primio
posla. Predlaže prije započinjanja ćorava posla proučiti izomorfizme,
sistemsku biologiju, teoriju kategorija, funkcionalne programske jezike,
umjetnu inteligenciju i naravno nakon tako dobivenih uvida sve lijepo
prevesti na jezik individualnih fantazija o bogaćenju. Ili ne. Nekako
već treba prevariti publiku koja rado miksa matematiku, ekonomiju i
zabavu. Ili ne. http://docs.google.com/View?id=dhjm83rs_98czntpn8n

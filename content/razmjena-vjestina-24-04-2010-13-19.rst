Razmjena vještina (24.04.2010. @13-19)
######################################
:date: 2010-04-23 14:45
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-24-04-2010-13-19
:status: published

Ove subote će prijatelji Emacsa doći na svoje - konačno se radi pregled
`org moda <http://orgmode.org/>`__. Za neupučene, org-mode je prošireni
tekstualni mod koji vam omogućava da kroz Emacs pišete prezentacije,
uređujete websajtove, vodite projekte, održavate vlastite GTD zabilješke
pa čak i vodite malo kućno računovodstvo ili ukratko org-mode daje
razloga da se i neprogrameri okušaju u užicima Emacsa. Vidimo se iza 13h

U predasima gledanja Emacsa ljubitelji vi-ja će updateati mljac, tako da
ako imate long-running procese u virtualnim mašinama tamo, možete
očekivati da ih nakon subote više neće biti ;-)

G33koskop: program za veljaču
#############################
:date: 2010-01-26 15:21
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-program-za-veljacu
:status: published

**Piratska stranka** uzbudila je mnoge duhove. Simbolika, vrckavost,
aktualnost, duhovitost i dovoljno malo vremena za još uvijek nikakvo
razočaranje lako mobilizira i u provincijama poput .hr

G33koskop će i ovaj put sačuvati zrno sumnje. Okupit ćemo predstavnike
studentskih blokada i (sad već) legendarnog plenuma, urbanih aktivista
Prava na grad, ekoaktivista što se upustiše u politiku, zainteresirane
za društvene promjene koje proizlaze iz politike ICT sektora. I tako.
Stipe Ćurković, Toni Vidan, Srđan Dvornik, Teodor Celakoski i drugi. +
Standardna G33koskop publika. Ekipa.

Dva vezana utorka diskutirat će o mogućnostima političke stranke s
jednom temom, (eventualnom) udruživanju takvih stranaka u klastere,
angažmanu bez (mogućnosti) velikog članstva, tehnokraciji, solidarnosti,
šbbkbb, nadama kakve stranke priželjkuju u budućnosti....

I gledat ćemo filmove naravno...

-  02.02.10@19:00
   **"Kako sam sistematski uništen od idiota"** -
   http://www.imdb.com/title/tt0085769/
   Za sve pjesnike, revolucionare i heroje revolucije. Prije revolucije.
-  09.02.10@19:00
   **"Milk"** - http://www.imdb.com/title/tt1013753/
   Za sve pjesnike, revolucionare i heroje revolucije. Prije revolucije.
-  16.02.10@19:00
   **Diskusija** o novim organizacijskim formama političkih stranaka
   Ekipa.
-  23.02.10@19:00
   **Diskusija** o novim organizacijskim formama političkih stranaka
   Ekipa.

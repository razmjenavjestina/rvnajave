HCRČ: TLB cachevi (09.03.2017. @ 19:30)
#######################################
:date: 2017-03-08 11:00
:author: nikolap
:category: hcrc
:status: published

Da li je O(1) zbilja uvijek konstanta? Kolika je stvarna cijena virtualnog
adresnog prostora? Zašto većina baza podataka traži dodatne privilegije? O ovim
i o mnogim drugim pitanjima bit će govora na sljedećem HCRČ-u gdje se bavimo
TLB cachevima - dragocjenom resursu o kojem se rijetko priča.

Počinjemo u 19:30 u Hacklabu u Mami. Vidimo se!

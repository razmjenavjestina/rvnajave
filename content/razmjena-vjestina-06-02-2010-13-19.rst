Razmjena vjestina (06.02.2010. @13-19)
######################################
:date: 2010-02-04 10:50
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-06-02-2010-13-19
:status: published

Evo nas već u veljači. U subotu ćemo, uz standardne aktivnosti,
analizirati Processing, programski jezik specijaliziran za pravljenje
slika, animacija i interaktivnog sadržaja. Ništa ambiciozno, tek da se
malo zabavimo. Vidimo se ...

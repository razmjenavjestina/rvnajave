Razmjena vještina (07.11.09. @13-19)
####################################
:date: 2009-11-06 08:34
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-07-11-09-13-19
:status: published

Za ovu subotu za sada nema ništa najavljeno, no vjerojatno ćemo
nastaviti sa aktivnostima od prošle sedmice: Qt toolchain i AVR/Arduino
from the scratch. Nije na odmet podsjetiti da se ovaj vikend u Zagrebu
održava 12. `CRŠ <http://www.crs-festival.com/>`__ tako da će neki broj
razmjenjivača tokom popodneva napustiti hacklab na jedno sat, dva.

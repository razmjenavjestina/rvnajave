impl Zagreb for Rust: Rust, FFmpeg i TensorFlow (09.10.2019. @ 18:00 h)
#######################################################################
:date: 2019-10-01 09:44
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201910
:status: published

Krećemo s novom sezonom Rust meetupa, ovaj put s vrlo interesantnom
kombinacijom Rusta, FFmpeg-a i TensorFlowa.

Klasika -- 18 h, Hacklab u MaMi.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/265307360/>`__.

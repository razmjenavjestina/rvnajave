Razmjena vještina (18.09.2010 @13)
##################################
:date: 2010-09-18 14:35
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-18-09-2010-13
:status: published

-  

   .. raw:: html

      <div>

   |Remove star| ex·change Noun   /iksˈCHānj/ |listen|

   .. raw:: html

      </div>

   .. raw:: html

      <div>

   .. raw:: html

      <div>

   Synonyms:

   .. raw:: html

      </div>

   -  verb:
      `interchange <http://www.google.com/dictionary?hl=en&q=interchange&sl=en&tl=en&oi=dict_lk>`__,
      `swap <http://www.google.com/dictionary?hl=en&q=swap&sl=en&tl=en&oi=dict_lk>`__,
      `swop <http://www.google.com/dictionary?hl=en&q=swop&sl=en&tl=en&oi=dict_lk>`__,
      `barter <http://www.google.com/dictionary?hl=en&q=barter&sl=en&tl=en&oi=dict_lk>`__,
      `commute <http://www.google.com/dictionary?hl=en&q=commute&sl=en&tl=en&oi=dict_lk>`__,
      `trade <http://www.google.com/dictionary?hl=en&q=trade&sl=en&tl=en&oi=dict_lk>`__,
      `replace <http://www.google.com/dictionary?hl=en&q=replace&sl=en&tl=en&oi=dict_lk>`__,
      `switch <http://www.google.com/dictionary?hl=en&q=switch&sl=en&tl=en&oi=dict_lk>`__
   -  noun:
      `interchange <http://www.google.com/dictionary?hl=en&q=interchange&sl=en&tl=en&oi=dict_lk>`__,
      `swap <http://www.google.com/dictionary?hl=en&q=swap&sl=en&tl=en&oi=dict_lk>`__,
      `swop <http://www.google.com/dictionary?hl=en&q=swop&sl=en&tl=en&oi=dict_lk>`__,
      `barter <http://www.google.com/dictionary?hl=en&q=barter&sl=en&tl=en&oi=dict_lk>`__,
      `replacement <http://www.google.com/dictionary?hl=en&q=replacement&sl=en&tl=en&oi=dict_lk>`__

   .. raw:: html

      </div>

   -  

      .. raw:: html

         <div>

      exchanges plural

      .. raw:: html

         </div>

   -  

      .. raw:: html

         <div>

      An act of giving one thing and receiving another (esp. of the same
      type or value) in return

      .. raw:: html

         </div>

      -  

         .. raw:: html

            <div>

         negotiations should eventually lead to an *exchange* of land
         for peace

         .. raw:: html

            </div>

      -  

         .. raw:: html

            <div>

         an *exchange* of prisoners of war

         .. raw:: html

            </div>

      -  

         .. raw:: html

            <div>

         opportunities for the *exchange* of information

         .. raw:: html

            </div>

   -  

      .. raw:: html

         <div>

      A visit or visits in which two people or groups from different
      countries stay with each other or do each other's jobs

      .. raw:: html

         </div>

      -  

         .. raw:: html

            <div>

         nine colleagues were away on an *exchange* visit to Germany

         .. raw:: html

            </div>

   -  

      .. raw:: html

         <div>

      A short conversation; an argument

      .. raw:: html

         </div>

      -  

         .. raw:: html

            <div>

         there was a heated *exchange*

         .. raw:: html

            </div>

   -  

      .. raw:: html

         <div>

      The giving of money for its equivalent in the money of another
      country

      .. raw:: html

         </div>

   -  

      .. raw:: html

         <div>

      The fee or percentage charged for converting the currency of one
      country into that of another

      .. raw:: html

         </div>

   -  

      .. raw:: html

         <div>

      A system or market in which commercial transactions involving
      currency, shares, commodities, etc., can be carried out within or
      between countries. See also foreign *exchange*

      .. raw:: html

         </div>

   -  

      .. raw:: html

         <div>

      A central office or station of operations providing telephone
      service

      .. raw:: html

         </div>

      -  

         .. raw:: html

            <div>

         private branch *exchanges* to automate internal telephone
         networks

         .. raw:: html

            </div>

   -  

      .. raw:: html

         <div>

      A move or short sequence of moves in which both players capture
      material of comparable value, or particularly (the *exchange*) in
      which one captures a rook in return for a knight or bishop (and is
      said to win the *exchange*)

      .. raw:: html

         </div>

   -  

      .. raw:: html

         <div>

      A building or institution used for the trading of a particular
      commodity or commodities

      .. raw:: html

         </div>

      -  

         .. raw:: html

            <div>

         the New York Stock Exchange

         .. raw:: html

            </div>

.. |Remove star| image:: http://www.google.com/dictionary/image/starred.gif
.. |listen| image:: http://www.google.com/dictionary/flash/SpeakerOffA16.png
   :width: 16px
   :height: 16px
   :target: http://www.gstatic.com/dictionary/static/sounds/de/0/exchange.mp3

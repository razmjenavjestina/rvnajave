G33koskop: (24.05.2012. @19:00): Branimir Makanec: Rani dani
############################################################
:date: 2012-05-22 03:12
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-24-05-2012-1900-branimir-makanec-rani-dani
:status: published

Već se dugo na g33koskopu nismo bavili arheologijom digitalnog nasljeđa.

Nakon Damira Muraje, Miroslava Kocijana, Voje Antonića naš sljedeći gost
biti će Branimir Makanec, pionir primjene računala u edukaciji, koji je
još daleke 1961. godine osnovao sekciju Narodne tehnike pod nazivom
"Grupa kibernetičara", 1968. osnovao zagrebački Multimedijski centar,
kasnije patentirao i implementirao sistem komunikatora za frontalnu
programiranu nastavu, stroj za individualnu programiranu nastavu “Ines”,
Projektirao je i izveo oko 15 feed-back učionica u raznim gradovima
tadanje Jugoslavije, kao šef razvoja poduzeća "Ivasim" projektirao je
prvo hrvatsko personalno računalo "Ivel Ultra". Dobitnik je državne
nagrade za životno djelo "Faust Vrančić" (1997.), nagrade za životno
djelo Zagrebačke zajednice tehničke kulture "Dr.Oton Kučera" (2001.),
kao i nagrade za životno djelo "Dr. Albert Bazala" (2002.).

Respect.

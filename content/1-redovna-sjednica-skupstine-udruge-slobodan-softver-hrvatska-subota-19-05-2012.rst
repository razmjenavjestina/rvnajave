1. Redovna sjednica Skupštine udruge "Slobodan Softver Hrvatska" subota, 19.05.2012.
####################################################################################
:date: 2012-05-14 02:16
:author: drgspot
:category: Uncategorized
:tags: fsf-hr, slobodan softver hrvatska, ssh
:slug: 1-redovna-sjednica-skupstine-udruge-slobodan-softver-hrvatska-subota-19-05-2012
:status: published

| 1. Redovna sjednica Skupštine udruge "Slobodan Softver Hrvatska"
| održati će se subotu, 19. svibnja 2012. s početkom u 11:00 sati u Net
  Kulturnom Klubu "MaMa" / hackLab u mami, Preradovićeva 18, Zagreb.

http://lists.iugrina.com/pipermail/fsf-hr/2012-May/000742.html

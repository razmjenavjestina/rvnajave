Razmjena vještina (05.12.09. @13-19)
####################################
:date: 2009-12-04 12:16
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-04-12-09-13-19
:status: published

Kako su nam se zaredala dva NSND-a u dvije sedmice većina razmjenjivača
se štedi ne bi li svoje umotvorine prezentirali na "službenim"
događajima. No bilo kako bilo, nešto će se ipak odvijati. Ako ništa,
uvodi se nova wireless infrastruktura u klubu pa bi svi kojima su bliže
sistemaške teme mogli nešto i naučiti. A bit će i kave i napolitanki ...

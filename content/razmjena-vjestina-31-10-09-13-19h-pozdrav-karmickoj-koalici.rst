Razmjena vještina (31.10.09. @13-19h): pozdrav Karmičkoj Koalici
################################################################
:date: 2009-10-30 10:30
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-31-10-09-13-19h-pozdrav-karmickoj-koalici
:status: published

Uz standardne razmjenjivačke aktivnosti koje su već same po sebi
dovoljan razlog da si upropastite planove za sve subote narednih 20
godina, ovaj put ćemo imati i nešto službeniji dio programa: Bubuntu Bud
predstavlja Karmic Koalu. Ma što mislili o svome Debianu (Gentuu, Archu,
...) Budov pristup će vas natjerati da se pokolebate. Najtoplije
preporuke!

Linux Install Fest & Linux Konferencija DORS/CLUC
#################################################
:date: 2010-04-28 00:17
:author: drgspot
:category: Uncategorized
:tags: kset ubuntu linux installfest linux conference dors cluc
:slug: linux-install-fest-linux-konferencija-dorscluc
:status: published

Cet 29.4. odrzat ce se Linux InstallFest, u KSET-u

| Bit ce predstavljena nova verzija Ubuntu 10.04.
| Organizira: Računarska sekcija KSET-a, u suradnji s Hrvatskom udrugom
  Linux korisnika te Udrugom Ubuntu korisnika u Hrvatskoj.

Kratko predavanje ce zapoceti u 11:oo u Sivoj vijećnici FER-a, a
installFest ce se nastaviti u KSET-u u ugodnom druzenju, razmjeni
iskustva i ucenju.

http://www.kset.org/news/id/41/

--------------

Blizi se Linux Konferencija DORS/CLUC 2010....

Fer, 5.-7.05.2010.

http://www.open.hr/dc2010/program.php

http://www.open.hr/dc2010/

http://cluc.linux.hr/

Prosla godina:

http://www.youtube.com/watch?v=gZMWc8PUOvs

drGpoint

G33koskop.ext (13.10.2010. @18:00): Voja Antonić i Davor Emard o bežičnom prijenosu energije
############################################################################################
:date: 2010-10-12 16:40
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-ext-13-10-2010-1800-voja-antonic-i-davor-emard-o-bezicnom-prijenosu-energije
:status: published

Voja Antonić čest je gost TV-emisija koje se trude **mambodžambu**
suprostaviti nešto razuma. Davorovo ponavljanje Teslinih eksperimenata
izaziva kontroverze. No Davor i dalje marljivo razvija svoje
prototipove. Mi u hacklabu volimo mahati bez veze svijetlećim neonkama.
Voja i Davor će razgovarati o bežičnom prijenosu električne energije. Mi
ćemo mahati neonkama. *Star wars* na extended g33koskopu srijedom
popodne.

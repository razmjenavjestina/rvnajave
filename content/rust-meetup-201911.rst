impl Zagreb for Rust: Proceduralni makroi (14.11.2019. @ 18:00 h)
#################################################################
:date: 2019-11-05 11:06
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201911
:status: published

Rust meetup jaše dalje, ovaj put uz Akino predavanje o proceduralnim makroima.
Pričat ćemo o tome kako oni izgledaju u Rustu, ali i o tome kako stoje u
usporedbi sa sličnim featureima drugih jezika.

Počinjemo u tradicionalnih 18 h u Hacklabu u MaMi.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/266226748/>`__.

Ubuntu Global Jam (04.10.09. @18h)
##################################
:date: 2009-10-12 13:49
:author: admin
:category: Uncategorized
:tags: najava
:slug: ubuntu-global-jam-04-10-09-18h
:status: published

|image0|

| Udruga ubuntu-hr nalazi se ove nedjelje u prostoru net kluba
  `MaMa <http://mama.mi2.hr/>`__ kako bi obilježila `Ubuntu Global
  Jam <https://wiki.ubuntu.com/UbuntuGlobalJam>`__. Taj dan obilježit
  ćemo testiranjem nadolazeće verzije ubuntu-a `Karmic
  Koala <http://www.ubuntu.com/testing/karmic/beta>`__ na par računala,
  te prevoditi pakete za nadolazeću verziju.
| E da, svi su dobrodošli, naravno! Eto ukratko o nedjelji, vidimo se!

.. |image0| image:: http://www.ubuntu-hr.org/wp-content/uploads/2009/10/ugj.png
   :class: alignnone
   :width: 200px
   :height: 131px

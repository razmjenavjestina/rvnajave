Razmjena vještina (19.12.09. @13-19)
####################################
:date: 2009-12-16 12:07
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-19-12-09-13-19
:status: published

Sve već znate - velika većina razmjenivača će vikend provesti na
moravičkom NSND-u tako da nas čeka nešto mirnija subota uz kavicu,
ćaskanje i meditativno konfiguriranje laptopa. Ukoliko ste neodlučni, za
ovu subotu preporučamo Moravice.

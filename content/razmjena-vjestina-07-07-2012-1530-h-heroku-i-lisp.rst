Razmjena vještina (07.07.2012. @ 15:30 h): Heroku i Lisp
########################################################
:date: 2012-07-06 14:53
:author: nplejic
:category: razmjenavjestina
:slug: razmjena-vjestina-07-07-2012-1530-h-heroku-i-lisp
:status: published

*Arhiva svih
videa: \ http://www.youtube.com/user/razmjenavjestina/videos*

Razmjene vještina subotom ponovno kreću. Plan i program slijedi:

15:30 h -- Crowd Programming
----------------------------

U duhu `pair
programminga <http://en.wikipedia.org/wiki/Pair_programming>`__ krenuli
bismo sa zajedničkim radom gdje driver/vozač vozi na projektoru, a
promatrači navigiraju.

U planu je baviti se Clojureom i
`Overtoneom <http://overtone.github.com/>`__.

17:30 h -- Kazimir Majorinc: Lisp
---------------------------------

Kazimir će držati prvo u seriji polusatnih predavanja o Lispu. Teme
izlaganja bi bile najvažniji materijali o Lispu, uglavnom članci,
ponekad knjige - koliko je moguće popularno i historijskim redom, od
kraja 1950-ih pa naovamo.

19:00 h -- Ivan Stojić: Heroku
------------------------------

Heroku je *cloud* platforma za hosting web aplikacija koja podržava
nekoliko programskih jezika. Ivan će nam objasniti što se može s
Herokuom, s naglaskom na općenito korištenje neovisno o jeziku, a ovisno
o interesu prisutnih postoji mogućnost osvrta na node.js i Python.

20:00 h -- Vladimir Klemo: priprava i uživanje u hrani
------------------------------------------------------

Nakon svega ćemo uz pomoć Kleme pokušati nešto skuhati i uživati u
dotičnom.

Predavanja će biti snimana i streamana na Google+ Hangoutu. Više
informacija snimkama i streamu naknadno.

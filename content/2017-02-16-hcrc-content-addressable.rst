HCRČ: Content Addressable Document Storage (16.02.2017. @ 19:30)
################################################################
:date: 2017-02-16 12:30
:author: nikolap
:category: hcrc
:status: published

U ovom izdanju HCRČ-a ćemo pokušati implementirati jednostavni, minimalni alat
za izlaganje dokumenata preko HTTP-a adresiranih po njihovom sadržaju. Ukratko,
uzet ćemo skup dokumenata u nekom direktoriju i učiniti ih dostupnima preko
njihovog hasha, pa vidjeti kamo će nas to odvesti.

Za sada je sigurno da će Marcell to probati napraviti u Pythonu, a Nikola u Lui
/ OpenRestyju. Dodatne implementacije dobrodošle.

Počinjemo u 19:30 u Hacklabu u Mami. Vidimo se!

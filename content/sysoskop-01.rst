SyS'o'Skop 01
#############
:date: 2012-02-15 15:29
:author: mans
:category: Uncategorized
:slug: sysoskop-01
:status: published

Nakon 2 tjedna stigao nam je i idući SyS'o'Skop koji se održava u Subotu
18.02 u 16:00.  Kao i do sada igramo na kartu dogovora na listu mjesta
za\ `predložene
teme <http://wiki.razmjenavjestina.org/index.php/Sys%27o%27skop>`__.  I
ovaj put očekujemo dobar odaziv sistemaca i svih onih koji se osjećaju
sistemcima. :) Sve ono što se radilo na prijašnjem SyS'o'Skopu možete
pročitati \ `ovdje <http://bljak.org:9001/ro/r.job3irS7KyxOVnWB>`__.

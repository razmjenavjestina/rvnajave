impl Zagreb for Rust: Uvod u lock-free Rust (23.01.2020. @ 18:00 h)
#############################################################################
:date: 2020-01-08 09:40
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-202001
:status: published

I u novoj godini nastavljamo s Rust meetupovima -- ovaj put nas Hrvoje Nikšić
uvodi u lock-free Rust.

Vidimo se 23.1. u tradicionalnih 18 h u MaMi!

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/267742601/>`__.

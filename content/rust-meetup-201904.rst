impl Zagreb for Rust: Persistent data in Rust (04.04.2019. @ 18:00 h)
#####################################################################
:date: 2019-03-27 14:56
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201904
:status: published

The third Rust meetup is dedicated to data persistence, mostly through the
prism of database adapters and ORMs for relational database systems.

This time we'll have a talk in English since we might have people who don't
understand materinji.

Further details on the `Meetup page
<https://www.meetup.com/Zagreb-Rust-Meetup/events/259597646/>`__.

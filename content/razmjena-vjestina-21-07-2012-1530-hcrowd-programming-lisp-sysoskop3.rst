Razmjena vještina (21.07.2012 @ 15:30 h): Crowd programming, Lisp, SyS'o'skop3
##############################################################################
:date: 2012-07-19 13:54
:author: mans
:category: Uncategorized
:slug: razmjena-vjestina-21-07-2012-1530-hcrowd-programming-lisp-sysoskop3
:status: published

*Arhiva svih
videa: \ http://www.youtube.com/user/razmjenavjestina/videos*

Još jedna razmjena vještina i ovaj put u Vašem gradu u Hacklabu u Mami.
Program slijedi:

15:30 — Crowd Programming
-------------------------

U duhu \ `pair
programminga <http://en.wikipedia.org/wiki/Pair_programming>`__ krenuli
bismo sa zajedničkim radom gdje driver/vozač vozi na projektoru, a
promatrači navigiraju.

Točna tema još nije poznata. Prijedlozi dobrodošli.

17:30 — Kazimir Majorinc: Lisp
------------------------------

Nakon vrlo uspješnog prošlotjednog predavanja, Kazimir nastavlja sa
svojom serijom o Lispu. Teme izlaganja bi bile najvažniji materijali o
Lispu, uglavnom članci, ponekad knjige – koliko je moguće popularno i
historijskim redom, od kraja 1950-ih pa naovamo.

19:00 — Dobrica Pavlinušić
--------------------------

U duhu Sys'o'skopa nastavljamo sa `predloženim
temama <http://wiki.razmjenavjestina.org/Sys%27o%27skop>`__. Ovog puta
radimo `n2n <http://www.ntop.org/products/n2n/>`__ u praksi. Što je n2n?
Kako se koristi? Koje su dobre // loše strane  i kako ga implementirti?
Na sva ova pitanja odgovor bi trebali dobiti od Dobrice. Naravno jedna
od super stvari bi bila da dignemo supernode negdje i spojimo hacklabove
diljem balkana u jednu mrežu preko n2n sistema.

Predavanja će biti snimana i streamana na Google+ Hangoutu. Više
informacija snimkama i streamu naknadno.

SIG:GIS (16.11.2009. @17:30) ZG OSM drugo službeno okupljanje
#############################################################
:date: 2009-11-12 11:57
:author: dboto
:category: Uncategorized
:tags: GIS:SIG, najava, Openstreetmap
:slug: siggis-16-11-2009-1730-zg-osm-drugo-sluzbeno-okupljanje
:status: published

Ovaj put u puno većem broju ZG OSM urednici se okupljaju u mami.  Uz
pregled dosadasnjih aktivnosti dogovarat ćemo prvi službeni mapping
party i pokušati definirati strategiju širenja zajednice. Ukoliko
dvorana bude prazna Dodobas će nas uputiti u sve tajne
`GNSS <http://en.wikipedia.org/wiki/GNSS>`__-a. Poslije idemo na pivo.

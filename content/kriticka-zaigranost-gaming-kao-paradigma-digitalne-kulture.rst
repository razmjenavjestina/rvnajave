simpozij “Kritička zaigranost” :: 21.01. u 18h
##############################################
:date: 2012-01-20 16:09
:author: drgspot
:category: Uncategorized
:tags: game culture multimedia hackLab mama economy
:slug: kriticka-zaigranost-gaming-kao-paradigma-digitalne-kulture
:status: published

Multimedijalni institut poziva Vas u subotu, 21. siječnja, od 18:00h u
klub za net.kulturu MAMA (Preradovićeva 18) na večernji simpozij o
računalnim igrama i digtalnoj kulturi:

**“Kritička zaigranost – *gaming* kao paradigma digitalne kulture”**

**>> Raspored:**

| 18:00-20:00
| **Katarina Peović Vuković:** *Virtually Real*
| **Matthew Fuller:** *Always One Bit More, Computing and the Experience
  of Ambiguity*

| 20:00-22:00
| **Kristian Lukić:** *Economies of Play*
| **Alessandro Ludovico:** *Hacking Playability, Disrupting Game Accepted Conventions*

| 
| **>> O simpoziju:**

“Nije čudno da digitalne igre postaju dominantna forma kulture našeg
vremena, u trenutku kada ta vremena sama predstavljaju niz manje
savršenih igara” [McKenzie Wark]

Nepovratno smo uronjeni u digitalno, pa i onda kada se čini da smo
offline životi su nam posredovani digitalnim. Prije deset godina,
početkom novog tisućljeća, činilo se da nema kraja kiberutopijama i
novim prostorima slobode koje su obećavale nove tehnologije. Društveni,
kulturalni i umjetnički imaginarij bili su opsjednuti figurama
nematerijalnog i bastardnog a DJ kao nova figura producenta
utjelovljavao je snove o beskonačno opuštenom miješanju, remiksiranju
svih postojećih kulturalnih znakova i obrazaca.

Vremena su tokom posljednjeg desetljeća zahladila. Digitalni
špekulativni mjehur se rasprsnuo na burzama, a globalni oružani sukobi
su nas podsjetili i na drukčije vrste geografija od onih koje su
obećavale kiberutopije. Smirenim strastima uprkos tehnološka je
penetracija išla dalje i za razliku od vremena kad se moglo kritizirati
‘kalifornijsku ideologiju’ [Richard Barbrook] više nije moguće iz prve
identificirati jedan kulturalni obrazac koji bi definirao nove prakse, 
kako je digitalni život postao zaista globalni fenomen sa nesvodivim
brojem varijacija.

To je ishodišna točka većeg broja teoretičara i umjetnika koji u
posljednje vrijeme pokušavaju odgovoriti na izazove digitalnog života u
dobu njegove distopijske realizacije. Za razliku od prijašnjih figura
DJa ili hackera koje su podgrijavale različite i brojne nade, ti
umjetnici i teoretičari govore o ‘igraču’ [engl. *gamer*], o homo ludens
digitalis-u kao novoj paradigmi digitalnog.  Igrač/gamer tu stoji za
jedno novo, otriježnjeno, ludičko i ne-eskapističko iskustvo preplitanja
digitalnog/analognog, utopijskog/distopijskog, virtualnog/realnog.

Provedite subotu u mami i saznajte više o računalnim igrama, o njihovom
značaju za razumijevanje prodora informacijskih tehnologija u društvene
procese, o gejmerskoj supkulturi, o zajednicama moddera, o indie
produkciji, o igrama kao umjetnosti i drugim zanimljivim aspektima ove
tematike.

Izlaganja će se održat na engleskom jeziku.

**>> O predavačima:**

**Katarina Peović Vuković** predaje na kulturalnim studijima na
Filozofskom fakultetu u Rijeci i urednica je književno-teorijskog
časopisa \ *Libra Libera*.

**Matthew Fuller** predaje na Centru za kulturalne studije na
Goldsmithsu. U pionirskim danima net.arta bio je član skupine I/O/D i
suradnik kolektiva Mongrel. Autor je više knjiga iz medijske
teorije: \ *Behind the Blip, Essays on the Culture of
Software*, \ *Media Ecologies – Materialist Energies in Art and
Technoculture* i dr.

**Kristian Lukić** je teoretičar, kustos i umjetnik, trenutno
istraživač-doktorand na Institut za znanstvena istraživanja pri
Fakultetu za društvena istraživanja u Beču. Suosnivač je Napona –
Instituta za fleksibilne kulture i tehnologije. Od 2001. do 2006. bio je
programski koordinator u Centru za nove medije\_Kuda.org. Suosnivač je
Eastwood – Real Time Strategy Group – skupine specijalizirane za
tematiku tehnologije i igre.

**Alessandro Ludovico** je medijski kritičar i od 1993. glavni urednik
nezaobilaznog časopisa za novomedijsku umjetnost, elektroničku muziku i
haktivizam \ *Neural*. Jedan je od početnih kontributora Nettime
zajednice. Predaje na umjetničkoj akademiji u Carrari.

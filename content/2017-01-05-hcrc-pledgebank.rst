HCRČ: Pledgebank (05.01.2017. @ 19:30)
######################################
:date: 2017-01-04 18:48
:author: nikolap
:category: hcrc
:status: published

`Pledgebank <http://www.pledgebank.com/>`__ je izvrsna ideja i nešto lošija
implementacija kojoj ne ide u prilog i to da su odustali od daljnjeg
razvoja. Zvuči kao esencija onoga kako internet može pomoći ljudima:
("jeftino") povezati one koji imaju istu potrebu ili isti cilj i tako im
pružiti priliku da nešto zajedno učine.

Raspon što se sve na takvom servisu može dogoditi je lako zamisliti: od
jednostavnog scenarija čišćenja parka ispred zgrade ako se pridruži n
susjeda do toga da ću glasati za novu stranku/nezavisnu listu na
lokalnim izborima ako mi se pridruži nekoliko desetaka hiljada sličnih
(isfrustriranih i svjesnih da ne treba puno glasova za ući u gradske
skupštine, ali da je i to teško skupiti).

Kroz Razmjenu bismo pokušali napraviti dostojnog nasljednika ovom pokojnom
servisu. Sutra, 05.01.2017. na HCRČ-u, bismo postavljali arhitekturu i
raspravili implementacijske korake.

Počinjemo u 19:30 u Hacklabu u Mami. Vidimo se!

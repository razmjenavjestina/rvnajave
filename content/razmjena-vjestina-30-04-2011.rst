Razmjena vještina (30.04.2011)
##############################
:date: 2011-04-30 12:15
:author: bud
:category: razmjenavjestina
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-30-04-2011
:status: published

Pozdrav razmjenjivači!

Povodom nedavnog izlaska nove inačice popularne Ubuntu Linux
distribucije, ovaj put pod nazivom Natty Narwhal (11.04), održat ćemo
mali install party, odnosno sprašit novi Ubuntu onome tko je još uvijek
na staroj verziji :).

Eto toliko, vidimo se!

Razmjena vještina (24.10.09. @13-19h)
#####################################
:date: 2009-10-20 14:35
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-24-10-09-13-19h
:status: published

U subotu se odvija povijesna razmjena: razmjenjivači dobivaju ključeve
kluba i službeno se otvara hacklab. Naravno, kako niti jedan početak
nije lagan, umjesto slavlja čeka vas izvjesna količina fizičkog rada.
Vidimo se točno u 13h.

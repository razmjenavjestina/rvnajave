Razmjena vještina (08.01.2011 @18)
##################################
:date: 2011-01-07 14:21
:author: lovro
:category: Uncategorized
:slug: razmjena-vjestina-08-01-2011-18
:status: published

Oslobađamo još jedan kod.

Nakon `OpenBSC <http://openbsc.osmocom.org>`__-a na baznim stanicama
Harald Welte presao je na GSM base band stack na mobitelima sa
`Osmcom-bb <bb.osmocom.org>`__ -om.

Naostrili smo lemilice, kupili mobitele, napravili kablove, pa da vidimo
kako GSM mreze rade.

Vidimo se!

G33koskop (22.10.2011. @20:00): James Vasile: FreedomBox - Sloboda u kućištu
############################################################################
:date: 2011-10-18 17:20
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: freedombox
:status: published

U subotu, 22.10. u 20h u Hacklabu u klubu MAMA, Preradovićeva 18,
**`James Vasile <http://hackervisions.org/?page_id=3>`__** će govoriti o
**`FreedomBoxu <http://freedomboxfoundation.org/>`__**, maloj autonomnoj
serverskoj infrastrukturi za svačiji dom koja ima za cilj omogućiti
anonimnost i zaštiti privatnost komunikacije, nadasve u situacijama kada
aktivisti i disidenti u brobi protiv represivnih režima trebaju
decentraliziranu, anonimnu i šifriranu razmjenu informacija i kanale
obraćanja javnosti.

*O predavanju
*\ **James Vasile: FreedomBox - Sloboda u kućištu**

Ova prezentacija je opći uvod i izvještaj o trenutnoj fazi razvoja
projekta “FreedomBox" - projekta kojeg podupire neprofitna organizacija
FreedomBox Foundation čiji je izvršni direktor James, a utemeljitelj
Eben Moglen. FreedomBox je osobno serversko računalo koje dolazi sa
slobodno-softverskim operativnim sistemom i aplikacijama, a koji je
projektiran s ciljem da omogući i osigura osobnu privatnostu korisnika
služeći im kao sigurna računalna platforma na kojoj mogu graditi
distribuirane društvene mreže. Softver za FreedomBox sastavljaju
programeri-volonteri iz čitavog svijeta koji vjeruju u slobodni softver
i slobodno društvo.

**Što je FreedomBox?:**

-  e-mail i telekomunikacijske tehnologije koje štite privatnost i
   onemogućuju prisluškivanje;
-  platforma za objavljivanje sadržaja koja zaobilazi represiju i
   cenzuru javnog govora;
-  organizacijska alatka za prodemokratske aktiviste u represivnim
   režimima;
-  komunikacijska mreža koja može služiti u hitnim situacijama krize.

FreedomBox ljudima u ruke i pod njihovu kontrolu stavlja šifriranu
glasovnu i tekstualnu komunikaciju te anonimni sistem objavljivanja,
društveng umrežavanja, dijeljenja medijskih sadržaja i
(mikro)blogiranja. Većina potrebnog softvera već postoji:
`TOR <http://www.torproject.org/>`__ za anonimni pristup mreži,
`GNUPG <http://www.gnupg.org/>`__ za šifriranje komunikacije, virtualne
privatne mreže itd. Postoje već i malena, niskovatna računala poznata
kao "server na utičnici" (plug server) na kojima se taj softver može
vrtiti. Teži dio zadatka je integrirati te tehnologije, distribuirati ih
i olakšati njihovu primjenu bez potrebe za stručnom pomoći. Također
potrebno je i decentralizirati njihovu distribuciju i pristup resursima
tako da korisnici ne moraju ovisiti i vjerovati centraliziranoj
infrastrukturi.

Upravo je to zadatak FreedomBoxa: mi integriramo zaštitu privatnosti
upotrebom jeftinih "servera na utičnici" ne bi li svakome omogućili da
zaštiti svoju privatnost. Vaši podaci ostaju u vašem domu i po njima ne
mogu rudariti vlade, milijarderi, lupeži ili pak zabadala od susjeda.
Upotrebom FreedomBoxa u svom domu, bez obzira na tehnološke vještine,
možete uživati u sigurnoj, privatnoj i anonimnoj komunikaciji!

FreedomBox je suradnički projekt programera iz čitavog svijeta koji
vjeruju u slobodni softver i slobodno društvo. Mnogi od njih dolaze iz
zajednice razvijatelja `Debiana <http://www.debian.org/>`__, a mnogi iz
drugih kutaka svijeta informacijskih sloboda.

*O predavaču*

**James Vasile** je dugogodišnji korisnik i zagovornik tehnologija
dijeljenja i kreativnih medija. Izvršni je direktor u FreedomBox
Foundation, viši pravni savjetnik u `Software Freedom Law
Center <http://www.softwarefreedom.org/>`__ i član upravnog odbora
neprofitne organizacije `Open Source
Matters <http://opensourcematters.org/>`__ koja je nositelj
`Joomla <http://www.joomla.org/>`__ projekta. James je doktorirao pravo
na Columbia Law School, a prethodno je diplomirao politologiju i
ekonomiju na Fordham University. Također pisao je kod i dokumentaciju za
brojne FOSS softverske projekte.

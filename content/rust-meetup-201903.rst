impl Zagreb for Rust: Web servisi (07.03.2019. @ 18:00 h)
#########################################################
:date: 2019-03-01 07:59
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201903
:status: published

Na drugom Rust meetupu pričamo o web servisima. Napravit ćemo kratak pregled
ekosustava za web development, a koncentrirati se malo više na Actix-Web, jedan
od prominentnijih web frameworkova.

Počinjemo oko tradicionalnih 18 h.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/259356808/>`__.

Razmjena vještina (03.04.2010. @13-19)
######################################
:date: 2010-04-02 23:13
:author: bud
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-03-04-2010-13-19
:status: published

| Našli se Horza i ja na večernjoj pivici, upali u neke rasprave, te nam
  sinula ideja na koju smo se obojica nabrijali, te je odlučili sutra na
  razmjeni realizirati. Radi se o novom
  `LTSP-u <http://www.ltsp.org/>`__ (`Linux Terminal Server
  Project <http://www.ltsp.org/>`__ ), odlučili smo da ćemo na strojeve
  koju se nalaze u hacklabu posetirati zadnju aktualnu verziju
  `LTSP-a <http://www.ltsp.org/>`__, koji će biti upogonjen na beta
  verziji nadolazeće verzije `ubuntu-a <http://www.ubuntu.com/>`__, radi
  se o verziji
  `10.04 <http://www.ubuntu.com/testing/lucid/beta1#Introduction>`__,
  koje je ujedno i novi `LTS <https://wiki.ubuntu.com/LTS>`__ ubuntu.
| Više o cijeloj priči sutra na razmjeni.

Vidim ose!

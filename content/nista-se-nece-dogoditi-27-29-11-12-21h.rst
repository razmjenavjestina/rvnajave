Ništa se neće dogoditi (27.-29.11. @ 12-21h)
############################################
:date: 2009-11-24 01:18
:author: marcell
:category: Uncategorized
:tags: najava, nsnd
:slug: nista-se-nece-dogoditi-27-29-11-12-21h
:status: published

Od 27.11. do 29.11. u **Hacklabu u mami** održat će se još jedan
**`Ništa se neće dogoditi <http://www.nsnd.org>`__**. Neposredni povod
je proslava početka rada Hacklaba u mami. *W00fckng00t!*

Postoje neke stvari koje su teške za objasniti ne zato jer su
komplicirane i teške, već zato što su upravo suprotno: lake i
jednostavne. Takav je i **Ništa se neće dogoditi**. Radi. Svaki put do
sad. Kad god smo zbog lijenosti i opuštenosti propustili nešto
definirati **Ništa se neće dogoditi** je to na licu mjesta popravio.

Hacklab u mami `u Preradovićevoj
18 <http://maps.google.com/maps/ms?ie=UTF8&hl=en&msa=0&msid=109834080626780617753.00045705630ea2fa6ee81&ll=45.812365,15.97616&spn=0.005968,0.013797&z=16>`__
u Zagrebu od petka do nedjelje od 12:00 do 21:00 biti će to lice mjesta
što popravlja nenajavljeno.

Vidimo se.

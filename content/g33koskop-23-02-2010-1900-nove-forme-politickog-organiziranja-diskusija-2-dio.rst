G33koskop (23.02.2010. @19:00): Nove forme političkog organiziranja (diskusija 2. dio)
######################################################################################
:date: 2010-02-22 18:20
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-23-02-2010-1900-nove-forme-politickog-organiziranja-diskusija-2-dio
:status: published

Drugi dio. Ovaj put više o piratima, politici i tehnologiji, ekologiji.
Kako dalje? Ako dalje. Pirati ili nindže? Tko je bio prvi put, a ne dođe
drugi magarac/ica.

`Piratska
stranka <http://en.wikipedia.org/wiki/Pirate_Parties_International>`__
uzbudila je mnoge duhove. Simbolika, vrckavost, aktualnost, duhovitost i
dovoljno malo vremena za još uvijek nikakvo razočaranje lako mobilizira
i u provincijama poput .hr

G33koskop će i ovaj put sačuvati zrno sumnje. Okupit ćemo predstavnike
studentskih blokada i (sad već) legendarnog plenuma, urbanih aktivista
Prava na grad, ekoaktiviste što se upustiše u politiku, zainteresirane
za društvene promjene koje proizlaze iz politike ICT sektora. I
tako. Stipe Ćurković, Toni Vidan,Srđan Dvornik, Teodor Celakoski i
drugi. + Standardna G33koskop publika. Ekipa.

Dva vezana utorka diskutirat će o mogućnostima političke stranke s
jednom temom, (eventualnom) udruživanju takvih stranaka u klastere,
angažmanu bez (mogućnosti) velikog članstva, tehnokraciji, solidarnosti,
šbbkbb, nadama kakve stranke priželjkuju u budućnosti….

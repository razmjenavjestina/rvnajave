Drupal-HR i GIT (26.02.11 @17h)
###############################
:date: 2011-02-25 17:52
:author: bud
:category: razmjenavjestina
:slug: drupal-hr-i-git-26-02-11-17h
:status: published

Danas (25.2.) je veliki dan za Drupal zajednicu - uspješno je završena
migracija sa CVS (client-server) na Git (distributed) revision control
system.

Ako se pitate što je to uopće Git i nije vam jasno u čemu je kvaka,
niste jedini. I mi smo se to pitali i odlučili potražiti odgovor kod
kolega iz hacklaba gdje inače održavamo Drupal HR druženja.

Na pitanje što je to distributed revision control system, što mu to dođe
Git i zašto je toliko sexy (i mnogo toga još) ovu subotu će nam u sklopu
razmjene vještina odgovor dati Alan Pavičić (a.k.a. Aka).

Zato svakako ne propustite iznimnu priliku od pravih hakera doznati malo
više o cijeloj priči + nakon Akinog predavanja slijedi dio priče oko
Gita i Drupala.

Vidimo se sutra!

| Datum: 26.2.2011.
| Vrijeme: 17h
| Lokacija: hacklab mama @ Preradovićeva 18, ZG

| P.S.
| Isprike na okašnjeloj najavi, nadamo se da vas to neće spriječiti u
  dolasku!

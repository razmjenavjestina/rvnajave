Razmjena vještina (10.07.2010 @13-19)
#####################################
:date: 2010-07-09 10:14
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-10-07-2010-13-19
:status: published

Nogomet se bliži kraju, idemo nazad na posao. Osokoljeni ranom najavom
novog ` <http://lambda-the-ultimate.org/node/4009>`__\ `Mozillinog
jezika <http://lambda-the-ultimate.org/node/4009>`__\ kojeg već mnogi
(nas trojica) smatraju Go killerom, na subotnjoj razmjeni organiziramo
kolektivno čitanje Rust koda sa githuba. Ako ništa, iskoristite priliku
da naučite nešto o programskom jeziku kojega (još) nema na Wikipediji.

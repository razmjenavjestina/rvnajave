impl Zagreb for Rust: WebAssembly <3 Rust (02.05.2019. @ 18:00 h)
#################################################################
:date: 2019-04-28 12:49
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201905
:status: published

Dogurasmo do četvrtog Rust meetupa na kojem će nam Horki ispričati ponešto o
WebAssemblyju, sve ambicioznijoj i popularnijoj meti za Rust programe.

Počinjemo oko tradicionalnih 18 h.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/260942646/>`__.

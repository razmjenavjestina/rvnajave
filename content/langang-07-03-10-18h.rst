Langang (07.03.10. @18h)
########################
:date: 2010-03-05 09:57
:author: aka
:category: Uncategorized
:tags: langgang, najava
:slug: langang-07-03-10-18h
:status: published

Čeka nas ozbiljan lang-gang. Imamo potvrđene čak dvije velike
prezentacije i to obje od novih predavača: Veljko će nam pokazati
programiranje pod `OpenCL <http://en.wikipedia.org/wiki/OpenCL>`__ -om,
i to na NVIDIA driverima pod Linuxom, dok će nam Nikola dati sustavan
prikaz `CLR <http://en.wikipedia.org/wiki/Common_Language_Runtime>`__-a.
Novi ljudi, nove teme --  dragi moji ovo se ne propušta!

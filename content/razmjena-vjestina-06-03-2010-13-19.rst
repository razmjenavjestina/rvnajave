Razmjena vještina (06.03.2010. @13-19)
######################################
:date: 2010-03-05 09:56
:author: aka
:category: Uncategorized
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-06-03-2010-13-19
:status: published

Subota, dakle Razmjena. Kako se među razmjenjivačima počeo buditi duh
malog obrtništva odlučili smo za ovaj put napraviti sustavno
istraživanje mogućnosti `Amazonovih web
servisa <http://aws.amazon.com/>`__. Ne propustite posljednju priliku da
odoru cyber hippyja zamjenite Armanijevim odjelom, a još pritom pojedete
i pokoju napolitanku.

Razmjena vještina (12.11.2011)
##############################
:date: 2011-11-10 15:55
:author: marcell
:category: Uncategorized
:slug: razmjena-vjestina-12-11-2011
:status: published

Naš analitički tim uočio je da smo na kraju ciklusa nenajavljenih
razmjena vještina. Tome je pogodovalo nekoliko faktora no
najvjerojatnije preveliki broj instalacija Ubuntua (poznatog po nazivu
"sve radi (osim)").

Tim povodom najavljujemo temu djeljenje sadržaja na Internetu tzv.
[Share]. Vjerojatno najpoželjniji lanac je ulazni RSS feedovi, selekcija
RSS itema, selektirani izlazni RSS feeds. To je dugo vremena jako dobro
servisirao Google Reader. Ovih dana je Google ukinuo izlazne RSS
feedove, djeljenje, komentiranje i druge *social* funkcije unutar Google
Readera i naljutio dobar broj vjernih korisnika
(http://storify.com/marcell/google-reader-shitstorm, \ http://sharebro.org/links).

Marcell će pokazati što se sve moglo, što se sve može i što će se možda
moći u polju djeljenja sadržaja na internetu. Neka od subtnjih
spominjanja:

| `http://tt-rss.org
  http://www.newsblur.com/ <http://www.newsblur.com/>`__
| `http://www.instapaper.com <http://www.instapaper.com/>`__\ `
  http://wordpress.org/ <>`__
| http://posterous.com/
| `http://www.blogger.com <http://www.blogger.com/>`__
| http://readable.tastefulwords.com/
| http://www.readability.com/
| http://delicious.com/
| http://pinboard.in/
| http://sourceforge.net/projects/scuttle

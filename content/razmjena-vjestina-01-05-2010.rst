Razmjena vještina (01.05.2010)
##############################
:date: 2010-04-29 15:32
:author: dpavlin
:category: razmjenavjestina
:tags: najava, razmjenavjestina
:slug: razmjena-vjestina-01-05-2010
:status: published

Praznik rada razmjenjivači će provesti... radno.

Osim ispijanja kave i svakodnevnih doskočica, Dobrica će (iza 16 sati)
proći draft svojeg workshopa: *Kako napraviti Google od zgrade sa
računalima?*

Teme uključuju: network boot
(`PXElator <http://blog.rot13.org/pxelator/>`__), distribuirani
in-memory hash (`Sack <http://blog.rot13.org/sack/>`__), distribirano
procesiranje (`Gearman <http://gearman.org/>`__,
`Narada <https://launchpad.net/narada>`__) i ponešto o dokumentiranju
(`Sysadmin Cookbook <http://sysadmin-cookbook.rot13.org/>`__,
`bak-git <http://blog.rot13.org/bak-git/>`__).

Aka je obećao i kratak uvod u Ruby, tako da će praznik rada biti prava
fešta!

*iz pedagoških razloga se uvod u Ruby odgađa za sljedeću sedmicu (aka)*

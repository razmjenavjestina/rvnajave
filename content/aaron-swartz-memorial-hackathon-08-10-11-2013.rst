Aaron Swartz Memorial Hackathon (08.-10.11.2013.)
#################################################
:date: 2013-11-06 11:37
:author: nplejic
:category: Uncategorized
:slug: aaron-swartz-memorial-hackathon-08-10-11-2013
:status: published

Od 8. do 10.11.2013. se na nekoliko lokacija u svijetu održavaju
`memorijalni hakatoni <http://aaronswartzhackathon.org/>`__ u spomen na
Aarona Swartza, internet aktivista koji si je početkom godine oduzeo
život. Cilj je pokušati nastaviti njegov rad na otvorenosti
informacija. \ `Razmjena vještina <http://www.razmjenavjestina.org/>`__
se priključuje iz `Mame <http://www.mi2.hr/>`__. Možeš i
`ti <http://wiki.razmjenavjestina.org/images/b/bb/Ti.jpg>`__.

Više informacija na `wikiju Razmjene
vještina <http://wiki.razmjenavjestina.org/Aaron_Swartz_Memorial_Hackathons>`__.

G33koskop (20.10.09. @19h): Florian Cramer - Softver kao umjetnost
##################################################################
:date: 2009-10-19 15:07
:author: admin
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-20-10-19h-florian-cramer-softver-kao-umjetnost
:status: published

Programeri, hakeri, povjesničari umetnosti, kuratori ne pričaju isti
jezik. No čini se da se ponekad razumiju, još češće da se uopće na
razumiju. Florian Cramer govorit će o prirodi i genealogiji upravo tog
nesporazuma. Vaša pitanja nakon predavanja imaju priliku izboriti svoje
mjesto na vremenskoj osi bilježenja tih fenomena. Nesporazum ili sretni
susret? Nevažno, zajednički odlazak na pizzu je izvjestan.

Florianovim rječima:

*"The lecture will be a critical review of the discourse on software  as
art that was begun in the early 2000s, but has historical  precursors in
the early 1970s.*

*Media art and computer hacker traditions nominally converged in the
1990s and more substantially since the early 2000s. The question is
whether the resulting practices still count as art. The likely answer
for a contemporary art system that has re-embraced the notion of fine
art is, for sure: negative."*

Biografija ponovno vlastitim rječima:

"*My background is comparative literature and art history combined with
practical experience in software and copyleft culture, experimental
arts, poetics and aesthetics. My essay "Words Made Flesh", written in
2004 on a research fellowship at Piet Zwart Institute, sums up my larger
historical interest in the field, my German Ph.D. thesis
"Exe.cut[up]able Statements" elaborates it more specifically for the
history of literature and text-based art. All papers I published since
1996 are available under free licenses on my website
`http://cramer.pleintekst.nl:70 <http://cramer.pleintekst.nl:70/>`__*

.. raw:: html

   <div id=":2a6">

*. Since 2008, I am also a research  professor (Dutch: "lector")
coordinating the research programme  Communication in a Digital Age of
the Piet Zwart Institute.*"

.. raw:: html

   </div>

.. raw:: html

   <div>

za one koji žele više opisa florijanovim rječima:

.. raw:: html

   </div>

.. raw:: html

   <div>

http://docs.google.com/View?id=dhjm83rs\_777wdh4qcv

.. raw:: html

   </div>

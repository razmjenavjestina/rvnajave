Ništa se neće dogoditi (27.-29.11. @12:00) - specijalno izdanje
###############################################################
:date: 2009-10-27 11:47
:author: marcell
:category: Uncategorized
:tags: najava, nsnd
:slug: nista-se-nece-dogoditi-27-29-11-1200-specijalno-izdanje
:status: published

**Ništa se neće dogoditi** (*NSND*) kao i sedam puta ranije (Sarajevo,
Niš, Moravice, Split, Beograd) okupit će Razmjenjivače i druge
entuzijaste. U jutarnjem krugu dogovorit ćemo što razmjeniti ovaj put.
Igrajući se idejom da se najbolje stvari uvijek događaju van službenog
programa. Kako smo to već nekoliko puta okretali naopako, naopako
naopako bude.

Specijalno zagrebačko izdanje *NSND*-a je proslava otvaranja **Hacklaba
mama**. U sklopu *NSND*-a predstavit ćemo programe nastale na Razmjeni
vještina: G33koskop, Langgang i HCRC (HardCoreRazmjenaČetvrtkom).

Program (ili opis metodologije kako dolazimo do njega) na
http://nsnd.org

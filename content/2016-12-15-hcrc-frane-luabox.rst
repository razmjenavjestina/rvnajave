HCRČ: Luabox - aplikacijski wrapper za Luu (15.12.2016. @ 19:30)
################################################################
:date: 2016-12-14 11:40
:author: nikolap
:category: hcrc
:status: published

HCRČ je ponovo nervozan. Ovaj tjedan Frane II će nam govoriti o svom luaboxu -
generičkom Lua aplikacijskom wrapperu sa podrškom za module, automatsko
parsiranje argumenata, shell completion i još štošta. Osim zbog same
biblioteke, projekt je zanimljiv i zbog inovativnoga načina exception handlinga
koji ima potencijal da se izdvoji iz samog luaboxa i postane biblioteka za
sebe.

Počinjemo u 19:30 u Hacklabu u Mami. Vidimo se!

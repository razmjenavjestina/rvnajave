Otvoreno i slobodno upravljanje sadrzajem (cms)
###############################################
:date: 2011-01-10 11:19
:author: drgspot
:category: Uncategorized
:tags: content managment system cms joomla
:slug: otvoreno-i-slobodno-upravljanje-sadrzajem-cms
:status: published

Otvoreno i slobodno upravljanje sadrzajem (cms)

Prvi susret Joomla! korisnika u Hrvatskoj :: SUBOTA, 15.01.2011.; 11:oo

http://en.wikipedia.org/wiki/Joomla

| Napokon se ekipa sa crojoomla.com foruma (i šire) odlučila okupiti i
| upoznati. Napokon ćemo moći u živo vidjeti tko sve te godine stoji sa
| druge strane tipkovnice. Ali nećemo se samo upoznavati nego i dijeliti
  svoja znanja. Susret će se sastojati od četiri predavanja i radionice
  u kojoj ćemo svi zajedno izgraditi Joomla site za samo 60 minuta.

| Za poslijepodne, u 17 sati najavljujemo još jedan uzbudljiv događaj:
| osnivanje udruge web sustava otvorenog koda. Ovo nikako neće biti samo
| "Joomla" udruga nego nam je cilj okupiti korisnike, developere i
| autore drugih web sustava otvorenog koda. Pozvani su svi drupalovci,
| worpressaši, SMFaši, phpBBaši i svi drugi opensoucaši!

Program susreta:

| 11:00 - 11:10 Uvodni govor /
| dobrodošlica (10 min)
| 11:10
| - 11:30 Predavanje:
| Uvod u Joomlu (20 min)
| 11:40
| - 12:10 Predavanje:
| Sigurnost Joomle u shared hosting okruženju (30 min)
| 12:20
| - 12:50 Predavanje:
| Joomla i SEO (30 min)
| 13:00
| - 13:20 Predstavljanje:
| Zadruga freelancera (20 min)
| - 16:40 Radionica:
| Joomla site u 60 minuta!
| (1 sat + 10 min pauze)
| 17:00
| - 19:00 Osnivačka skupština udruge (2 sata)

Više:

informacija na http://www.joomla-hrvatska.com

http://en.wikipedia.org/wiki/Joomla

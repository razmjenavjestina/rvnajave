impl Zagreb for Rust: Asinkrono programiranje u Rustu (11.12.2019. @ 18:00 h)
#############################################################################
:date: 2019-12-06 14:27
:author: nikola
:category: Rust
:tags: rust, najava
:slug: rust-meetup-201912
:status: published

Rust je napokon dobio sintaksnu podršku za asinkrono programiranje, ali je
ekosustav povelik i, barem izvana, teško razumljiv. Pokušat ćemo ga razjasniti.

Više detalja na `Meetup stranici
<https://www.meetup.com/Zagreb-Rust-Meetup/events/266996922/>`__.

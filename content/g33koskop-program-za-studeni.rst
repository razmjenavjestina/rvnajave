G33koskop: program za studeni
#############################
:date: 2009-10-27 11:18
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-program-za-studeni
:status: published

U prvom mjesecu rada hacklaba g33koskop se vraća korijenima - filmovima.
Ovaj mjesec je posvećen fanaticima, zanesenjacima, opsesivcima, luđacima
i njihovim zajednicama. I ne, nećemo se baviti organiziranim religijama
što kontroliraju državne budžete.  Ovaj put: jabuke, svemirska
putovanja, čudesna bižuterija te izležavanje na tepihu i kauču.

-  *03.11.09.@19:00*
   "**Macheads**" - http://www.imdb.com/title/tt1379667/

   .. raw:: html

      </p>
      <p>

   O opsjednutima svime što izađe iz Jabuke. Ovaj film prodaju izjavom
   sexblogerice Violet Blue koja kaže da nikad ne bi dala nekome tko
   koristi Mikromeke Prozore.

-  *10.11.09.@19:00*
   "**Trekkies**" - http://www.imdb.com/title/tt0120370/

   .. raw:: html

      </p>
      <p>

   O ljubavi i strasti prema zvjezdanim stazama. Zvjezdanim. Stazama.

-  *17.11.09.@19:00*
   "**Ringers: Lord of the Fans**" -
   http://www.imdb.com/title/tt0379473/

   .. raw:: html

      </p>
      <p>

   Debela knjiga. Mali prsten. Dugački film.

-  *24.11.09.@19:00*
   "**The Achievers: The Story of The Lebowski Fans**" -
   http://www.imdb.com/title/tt1276475/

   .. raw:: html

      </p>
      <p>

   Neda mi se baš previše pisat o ovom filmu. Radije bi malo odmorio. Pa
   pomalo.

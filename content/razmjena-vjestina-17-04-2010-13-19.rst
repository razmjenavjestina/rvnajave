Razmjena vještina (17.04.2010. @13-19)
######################################
:date: 2010-04-17 13:20
:author: horza
:category: razmjenavjestina
:slug: razmjena-vjestina-17-04-2010-13-19
:status: published

Razmjena počela u poznato uredovno vrijeme, a na njoj još nikoga.
Izgleda da bi najave valjalo pisati **prije** početka samoga događaja :)
Kako god, i danas ćemo se zasigurno okupiti u pristojnom broju, raditi
neke lijepe stvari i razmjenjivati vještine i zabavna iskustva.

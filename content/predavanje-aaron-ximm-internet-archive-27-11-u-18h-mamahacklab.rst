predavanje :: Aaron Ximm: Internet Archive :: 27.11. u 18h @ MAMA/Hacklab
#########################################################################
:date: 2012-11-27 00:26
:author: drgspot
:category: Uncategorized
:slug: predavanje-aaron-ximm-internet-archive-27-11-u-18h-mamahacklab
:status: published

.. raw:: html

   <div>

.. raw:: html

   <div>

MaMa i Hacklab u MaMi vas pozivaju u utorak 27.11. od 18h na predavanje:

.. raw:: html

   </div>

.. raw:: html

   <div>

**Aaron Ximm: Internet Archive (`archive.org <http://archive.org/>`__)**

.. raw:: html

   </div>

.. raw:: html

   <div>

—

.. raw:: html

   </div>

.. raw:: html

   <div>

**Internet
Archive** (`http://www.archive.org <http://www.archive.org/>`__) je
neprofitna digitalna knjižnica koja je utemeljna 1996. s misijom da
učini “opće dostupnim svo znanje”.

.. raw:: html

   </div>

.. raw:: html

   <div>

U cilju ostvarenja te misije Archive od sredine 90-ih arhivira
cijelokupni javno dostupni internet, kojemu se može pristupiti preko
sučelja Wayback Machine
(`http://web.archive.org <http://web.archive.org/>`__). Nedavno je
Archive krenuo i u misiju da poskenira i sačuva po primjerak svake
knjige koja je ikada tiskana na engleskom – a možda jednom čak i svake
knjige koja je uopće izdana. Iako nije jedini koji radi na tome, Archive
je jedina neprofitna institucija koja to čini u cilju javnog interesa i
javnog dobra – očuvanja digitalnih zajedničkih dobara.

.. raw:: html

   </div>

.. raw:: html

   <div>

Neki od istaknutijih projekata na kojima Archive trenutno radi je
pokretanje servisa za pretraživanje i posuđivanje televizijskih vijesti,
a koji se zasniva na naprednom pretraživanju opcionalnih titlova
televizijskih vijesti na engleskom jeziku s desetaka kanala koji se
snimaju 24 sata, 7 dana u tjednu. Također, prelazak na BitTorrent
protokol za peer-to-peer prijenos arhiviranih podataka (trenutno Archive
prati i dijeli više od dva milijuna torrenta).

.. raw:: html

   </div>

.. raw:: html

   <div>

Interesi Archivea često su srodni interesima poznatih tehnoloških
organizacija, između ostalog naših saveznika kao što su Electronic
Frontier Foundation, Wikimedia, Mozilla i Long Now Foundation.

.. raw:: html

   </div>

.. raw:: html

   <div>

—

.. raw:: html

   </div>

.. raw:: html

   <div>

**Aaron Ximm** je viši inženjer u Inernet Archiveu. Njegove glavne
zadaću su update zbirke Project Gutenberg, razvijanje infrastrukture za
distribuirani crawling domena od visokog značaja i visoke frekvencije
posjeta, izrade probnog koncepta osobnog digitalnog arhiviranja i, u
posljednje vrijeme, proširenje kapaciteta Internet Archivea i
distribucija preko BitTorrent protokola.

.. raw:: html

   </div>

.. raw:: html

   <div>

U paralelnom životu bavi se ambijentalnim zvučnim snimkama i zvukovnom
umjetnošću. Najpoznatiji je po  kompozitorskom, instalacijskom i
izvedbenom radu kao Quiet American, a većinu njegovog stvaralaštva
možete potržiti
na \ `http://www.quietamerican.org <http://www.quietamerican.org/>`__.

.. raw:: html

   </div>

.. raw:: html

   <div>

—

.. raw:: html

   </div>

.. raw:: html

   <div>

Gostovanje Aarona Ximma realiziramo uz pomoć Kiberpipinog festivala
HAIP, koji se 28-30.11. održava u Ljubljani na temu “Javna knjižnica –
funkcionalni prototip za budućnost”
(`http://2012.haip.cc <http://2012.haip.cc/>`__).
{copyLeft to-me}

.. raw:: html

   </div>

.. raw:: html

   </div>

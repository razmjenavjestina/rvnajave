Razmjena Vještina - IPv6 (2011-06-04)
#####################################
:date: 2011-06-03 18:21
:author: dpavlin
:category: razmjenavjestina
:slug: razmjena-vjestina-ipv6-2011-06-04
:status: published

Oduševljem radionicom Matije Nalisa na DORS/CLUC 2011, Dobrica je
nesmotreno obećao `ponoviti isti
sadržaj <http://linux.voyager.hr/ipv6/ipv6_dorscluc2011.html>`__ u mami.
Dakle, ako vas zanima IPv6, razgovarati ćemo o njemu nakon 17 sati. Cilj
je da svatko na kraju pošalje svoj prvi IPv6 paket (ako već nije, a
krajnje je vrijeme :-)

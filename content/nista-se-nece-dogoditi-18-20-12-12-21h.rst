Ništa se neće dogoditi (18.-20.12. @ 12-21h)
############################################
:date: 2009-12-16 14:45
:author: aka
:category: Uncategorized
:tags: najava, nsnd
:slug: nista-se-nece-dogoditi-18-20-12-12-21h
:status: published

.. raw:: html

   <div>

*Ništa se neće dogoditi u Moravicama po treći put.*

*Umjesto detalja, c/p maila:*

*Evo ekipa, konac godine se priblizava i vrijeme je za jos jedan NSND u
Moravicama. Za sve one koji su bili standard znate sta vas ceka i ove
godine, dobra hrana + snijeg + uobicajeni program jednog nsnd-a.
Moravice nisu promjenile lokaciju a ni kuhara |:)| Ovaj put ce i vecina
troskova biti pokrivena put/smjestaj/hrana. Dakle prijave su otvorene,
na svima vama je da se prijavite na listi od nsnd ili na mail
mans\ **AT**\ blacksystem\ **DOT**\ org . Vidimo se na NSND Moravice
3!!*

*ps: odgovaram na pitanja, ne odgovaram ako se izgubite |:)|*

*mans*

Uživajte!

.. raw:: html

   </div>

.. |:)| image:: http://www.nsnd.org/wp-includes/images/smilies/icon_smile.gif


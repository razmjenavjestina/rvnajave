G33koskop (08.11.2011. @19:00): Davor Emard: Egzotična energija piramida
########################################################################
:date: 2011-11-04 23:32
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-08-11-2011-1900-davor-emard-egzoticna-energija-piramida
:status: published

Davoru prvo ljudi ne vjeruju, pa nakon što se uvjere - vjeruju. Tako je
bilo sa bežičnim prijenosom energije.
`Bzzbzzbzzzbzzz <http://najave.razmjenavjestina.org/2010/10/12/g33koskop-ext-13-10-2010-1800-voja-antonic-i-davor-emard-o-bezicnom-prijenosu-energije/>`__ svjetlosnim
mačem i skeptici su začepali gubice. U novoj epizodi g33koskopa Davor
napada piramide. Bosanske. Bzzbzzbzzzbzzz...

Davor: "Jesu li piramide trebale biti toliko velike i imati savršenu
geometriju samo zato da bi im se drugi divili ili su morale biti
takve zbog neke puno praktičnije svrhe?

Jesu li služile kao prijemnici energije, mediji za komunikaciju sa
svemirom, laboratoriji za pokretanje života na Zemlji, sve zajedno ili
ništa od navedenog?

U Bosanskoj Dolini Piramida napravljena su mjerenja magnetskog polja
B-field antenom u potrazi za signalom upotrebljivim za bežični prijenos
energije i podataka."

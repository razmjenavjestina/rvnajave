G33koskop (26.01.10. @ 19:00): Davorin Ruševljan: Modeli organiziranja trgovanja na burzama i tehnologija – iskustva sa Zagrebačke burze
########################################################################################################################################
:date: 2010-01-25 17:56
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-26-01-10-1900-davorin-rusevljan-modeli-organiziranja-trgovanja-na-burzama-i-tehnologija-iskustva-sa-zagrebacke-burze
:status: published

| Mon3tizirani siječanj finiširat će iskusni Ruš. Opisat će osnovni
  model organiziranja trgovanja na burzama, te arhitekturu trgovinskog
  sustava MOST koji se na burzi koristio od 1999-2007. MOST je pisan u
  Smalltalku. Šećer za kraj.
| Update:

snimka predavanja

.. raw:: html

   <div>

.. raw:: html

   <object width="400" height="100">

.. raw:: html

   <embed src="http://s3.amazonaws.com/stlth/static/production/swf/audio_controller.swf" type="application/x-shockwave-flash" wmode="opaque" width="400" height="100" flashvars="song_label=converted-g33koskop_rush_burze_converted.mp3&amp;music_track=http://drop.io/download/public/47teoycp6jr20lbmy16p/c9368c4e36b830deff8a326dde44093ed177487f/9af5ae90-fa6f-012c-8e7a-fa968ebb7e14/47abcb20-fa74-012c-0545-f45d087ee257/v2/content&amp;autoplay=false">
   </embed>
   </object>

.. raw:: html

   </div>

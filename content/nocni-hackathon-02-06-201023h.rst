Noćni hackathon (02.06.2010@23h)
################################
:date: 2010-06-02 00:35
:author: aka
:category: Uncategorized
:tags: najava
:slug: nocni-hackathon-02-06-201023h
:status: published

U noći sa srijede na četvrtak organiziramo hackaton - cjelonoćni
programerski session gdje svatko radi na svojem projektu i uglavnom ne
komunicira s drugim sudionicima.

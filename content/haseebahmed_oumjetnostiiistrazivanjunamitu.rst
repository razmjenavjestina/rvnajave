G33koskop: (06.12.2011. @20:00): Haseeb Ahmed: O umjetnosti i istraživanju na MIT-u
###################################################################################
:date: 2011-12-05 05:46
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: haseebahmed_oumjetnostiiistrazivanjunamitu
:status: published

Prestižna mjesta je lako kritizirati, dovoljno je početi sa kritikom
svijeta prestiža koji predstavljaju. Nešto je teže kritizirati
visokosofisticirane narcisoidne tvorevine koje u realnom vremenu
absorbiraju svaku kritiku same sebe i trude se vanjski svijet
mobilizirati upravo u smjeru bavljenja njima samima.

Massachusetts Institute of Technology (MIT) je  visokosofisticirana,
prestižna obrazovna, istraživačka institucija. Sposobna provariti
kritiku same sebe u realnom vremenu. U svom istraživanju opsega pojma
geek (eng. scope of geek) g33koskop i Razmjena vještina već
tradicionalno ugošćavaju MIT istraživače: Benjamin Mako Hill, Star
Simpsons, Robert Ochschorn...

Predavanje mozete pratiti i
na: \ http://www.ustream.tv/channel/g33koskop

`Haseeb Ahmed <http://www.haseebahmed.com/>`__ je umjetnik, trenutno
istraživač na Jan van Eyck Akademiji u Maastrichtu, koji će predstaviti
svoj rad i iskustvo sa MIT-a kroz esej "O umjetnosti i istraživanju na
MIT-u".  Zabrinut "*of how to construct an artwork that will have enough
escape velocity to leave the orbit of the institute. If it fails to
achieve this ‘velocity’ it will be trapped in the orbit of MIT where,
even if ‘critical’, it would be of service for the ‘guilty conscience’
of the institute*", jer od svog osnutka MIT nije previše moralizirao oko
izvora investicija: od vojnih do korporativnih.

Haseeb uspješno izbjegava zamku moraliziranja, ali i cinizma, u svojoj
kritici, sasvim svjestan da ni MIT ne ostaje dužan. Haseeb nudi Marxa,
Adorna, tradiciju avangarde, umjetnički senzibilitet. MIT nudi želučanu
kiselinu zajebane institucije. No LOL, no boring, just deep. LOL.

| Haseeb Ahmed:
| `http://www.haseebahmed.com/
   <http://www.haseebahmed.com/>`__\ http://visualarts.mit.edu/people/grads/grad_ahmed.html

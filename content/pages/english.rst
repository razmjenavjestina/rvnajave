English
#######

Razmjena vještina (Skill Sharing) is an informal program of technology
enthusiats gathered around the `club MaMa <https://mi2.hr/>`__ and `Hacklab in
MaMa <https://wiki.hackerspaces.org/Hacklab_in_mama>`__.

Face to face meetups are sporadic. The best way to stay up to date and/or to
contact us is to join the IRC channel at `irc.libera.chat/#razmjenavjestina
<irc://irc.libera.chat/#razmjenavjestina>`__ (which has a `Matrix bridge
<https://matrix.to/#/#razmjenavjestina:libera.chat>`__ as well). 

The hacklab's location is `Preradovićeva 18, Zagreb
<https://osm.org/go/0IsmnvMoO?node=325725839>`__.

Our currently active projects are:

- `Rust Meetup <https://www.meetup.com/Zagreb-Rust-Meetup/>`__

Some of the older projects are documented on the `wiki
<https://web.archive.org/web/20170704013212/http://wiki.razmjenavjestina.org/Main_Page>`__.

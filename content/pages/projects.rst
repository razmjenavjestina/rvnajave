Projekti
########

Stariji projekti Razmjene vještina su uglavnom dokumentirani na `wikiju
<https://web.archive.org/web/20170704013212/http://wiki.razmjenavjestina.org/Main_Page>`__
i među `najavama <{index}>`__.

Trenutno aktivni projekti Razmjene su:

- `Rust Meetup <https://www.meetup.com/Zagreb-Rust-Meetup/>`__

I tvoj projekt može biti ovdje!

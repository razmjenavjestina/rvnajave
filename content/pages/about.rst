O Razmjeni
##########

:url: /
:save_as: index.html

Razmjena vještina je neformalni program tehnoloških entuzijasta okupljenih oko
`kluba MaMa <https://mi2.hr/>`__ i `Hacklaba u MaMi
<https://wiki.hackerspaces.org/Hacklab_in_mama>`__.

Okupljanja uživo su sporadična. Najbolji način za bivanje u toku i/ili kontakt
je praćenje IRC kanala `irc.libera.chat/#razmjenavjestina
<irc://irc.libera.chat/#razmjenavjestina>`__ (koji ima i `Matrix bridge
<https://matrix.to/#/#razmjenavjestina:libera.chat>`__). 

Hacklab se nalazi u `Preradovićevoj 18
<https://osm.org/go/0IsmnvMoO?node=325725839>`__.

Hacklab čita: B. Pierce - Software Foundations (10.09.2012. @ 19:30 h)
######################################################################
:date: 2012-09-09 13:06
:author: nplejic
:category: Uncategorized
:slug: b-pierce-software-foundations-10-09-2012-1930-h
:status: published

Počevši s 10.9., u hacklabu ćemo krenuti s čitanjem knjige Benjamina
Piercea i dr., `Software
Foundations <http://www.cis.upenn.edu/~bcpierce/sf/>`__. Knjiga se bavi
funkcijskim programiranjem, logikom te teorijom programskih jezika
(lambda računom, statičkim tipiziranjem, ...) kroz prizmu
`Coqa <http://coq.inria.fr/>`__, sustava za dokazivanje teorema koji je
ujedno i funkcijski programski jezik.

Nalazili bismo se jednom tjedno i diskutirali dijelove knjige i rješenja
zadataka. Ideja za prvi sastanak je:

-  upoznati se s knjigom,
-  namjestiti radno okruženje za Coq na omiljenim nam operacijskim
   sustavima,
-  dogovoriti način rada i plan i program za prvi tjedan.

Počinjemo u 19:30 h.

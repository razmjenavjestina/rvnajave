SyS'o'Skop 02
#############
:date: 2012-04-12 08:43
:author: mans
:category: Uncategorized
:slug: sysoskop-02
:status: published

Treći SyS'o'Skop po redu kreće u četvrtak 12.04.2012 od 17:00. Raspored
je isti kao i do sada - teme su
`ovdje <http://wiki.razmjenavjestina.org/index.php/Sys%27o%27skop>`__ te
vjerujemo da će se naći zanimljiva tema za sve koji dođu. Vidimo se
tamo.

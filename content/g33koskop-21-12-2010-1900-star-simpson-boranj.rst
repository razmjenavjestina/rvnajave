G33koskop (21.12.2010. @19:00): Star Simpson: Boranj
####################################################
:date: 2010-12-20 13:25
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: g33koskop-21-12-2010-1900-star-simpson-boranj
:status: published

Što god bila slijedeća velika stvar (en. next big thing) trebala bi se
rimovati sa naranča (en. orange). `Star
Simpson <http://starburst.hackerfriendly.com/>`__ se rimuje sa
`boranj <http://www.boranj.com>`__, a boranj se rimuje s 'naranča'. Star
Simpson pušta puno komaraca u veliku kutiju u kojoj ih onda `robot cvrči
laserom <http://www.youtube.com/watch?v=rD_eIutuGtE>`__, `crta 3D
animacije na kotaču
bicikla <http://www.youtube.com/watch?v=mT13ZcpwYtA>`__ koji se za taj
prizor mora upogoniti brzinom od barem 10km na sat, lemi, vari, žmirka i
tak. `MIT <http://www.eecs.mit.edu/>`__ voli pametne, `Intelectual
Ventures <http://intellectualventures.com/>`__ obožava inovativne,
aerodromska policija ih ponekad `hapsi i
maltretira <http://tv.boingboing.net/2008/09/19/star-simpson-once-mi.html>`__.
`Amerika <https://www.cia.gov/library/publications/the-world-factbook/geos/us.html>`__.

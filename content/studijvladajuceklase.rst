G33koskop: (11.05.2012. @19:00): Marcell Mars: Studij vladajuće klase 
######################################################################
:date: 2012-05-07 14:05
:author: marcell
:category: Uncategorized
:tags: g33koskop, najava
:slug: studijvladajuceklase
:status: published

Predmet istraživanja "Studij vladajuće klase" su poslovne strategije,
vizije, korporativne misije Googlea, Facebooka, Amazona i eBaya (GFAeB),
te dizajn i arhitektura njihovih tehnoloških infrastruktura, politika
pristupa i upravljanja korisničkim servisima i pripadajućim podacima kao
i aproprijacija kontrakulturnih, najčešće hakerskih, vrijednosti i
identiteta. Cilj istraživanja je uspostavljanje metodologije i kritičkog
teorijskog instrumenta za analizu i evaluaciju političke, etičke, i
profesionalne odgovornosti navedenih korporacija prema njihovim
(proklamiranim) misijama i (reklamiranim) praksama u svijetu u kojem
njihovi monopoli zauzimaju sve veći prostor.

Svaki od GFAeB su pokrenuti kao poslovi vođeni isključivo na Internetu.
Oni su poslovni domoroci digitalnih mreža. Njihove mrežne infrastrukture
naivnom promatraču izgledat će kao "javno dostupni servisi", upravljani
dinamikom njihovih "građana", ostvarenje koje bi očekivali od države
koja brine o zajedničkim resursima. Njihov golemi rast je paradigmatski
primjer snage i efekta mrežnog razvoja. Mnogi su u ranim danima
digitalnih mreža sanjali o eliminaciji posrednika u poslovanju i
komunikaciji, ali iako su brojni posrednici otpali, oni koji su opstali
učvrstili su svoje monopole s daljnjom tendencijom dominacije.

| Oni digitaliziraju svjetsku intelektualnu baštinu, planetu Zemlju,
  Mjesec, mora i podmorje (Google), osiguravaju globalnu platformu
  trgovanja gdje bilo tko može prodati bilo što bilo kome (eBay),
  facilitiraju ponudu na tržištu intelektualnog rada tako što spajaju
  poslodavca s najamnim radnikom bez direktnog kontakta gdje je posao
  podjeljen u hiljade malih zadataka koje radnicima disribuira online
  platforma u realnom vremenu (Amazonov Mechanical Turk). Oni znaju tko
  je prijatelj s kim (Facebook), kakve su čije potrošačke preferencije
| (Amazon) ili koliko je tko pouzdan prodavač ili kupac (eBay). Oni su
  vladajuća klasa digitalnog svijeta.

| Dosadašnji rezultati istraživanja su mapiranje i mjerenje fizičke
  infrastrukture Google-a: koliko servera, koliko novaca, ljudi i
  vremena je potrebno za izgradnju takvog sustava, povijesni pregled
  razvoja i implementacije ideje virtualne mašine, ispreplitanja
  različitih socijalnih dinamika (vojne, korporativne, akademske i
| kontrakulturne) u pokušaju ostvarivanja autonomije kroz kontrolu
  razvoja virtualne mašine i konkretan razvoj slobodnog softvera i
  izgradnja autonomne mrežne infrastrukture p2p biblioteke ("ilegalnih")
  digitalnih knjiga.

Link: http://www.scribd.com/my\_document\_collections/3523982

| - - - -
| O predavaču:

| Nenad Romić aka Marcell Mars (r.1972.). Napredni korisnik Interneta.
| http://ki.ber.kom.uni.st/
